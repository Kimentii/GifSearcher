package com.qulix.gifsearcher

import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.ProgressBar
import android.widget.TextView
import com.qulix.gifsearcher.data.Gif
import java.io.InputStream

class GifGalleryFragment : Fragment() {

    private lateinit var gifsRecyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var imageDownloader: ImageDownloader<GifImageView>
    private var fetchItemsTask: FetchItemsTask? = null
        @Synchronized get() = field
        @Synchronized set(value) {
            field = value
        }
    private val isLoading: Boolean
        get() = fetchItemsTask != null

    companion object {
        private val TAG = GifGalleryFragment::class.java.simpleName
        fun newInstance(): GifGalleryFragment {
            return GifGalleryFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        val responseHandler = Handler()
        imageDownloader = ImageDownloader(responseHandler)
        imageDownloader.imageDownloadListener = { target: GifImageView, image: InputStream ->
            target.imageInputStream = image
        }
        imageDownloader.start()
        imageDownloader.looper
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_gif_gallery, container,
                false)
        gifsRecyclerView = view
                .findViewById(R.id.fragment_photo_gallery_recycler_view)
        progressBar = view
                .findViewById(R.id.progress_bar)
        val linearLayoutManager = LinearLayoutManager(activity)
        gifsRecyclerView.layoutManager = linearLayoutManager
        gifsRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!isLoading) {
                    val totalItemsCount = linearLayoutManager.itemCount
                    val lastVisibleItem = linearLayoutManager.findLastCompletelyVisibleItemPosition()
                    if (totalItemsCount == (lastVisibleItem + 1)) {
                        updateItems(totalItemsCount)
                    }
                }
            }
        })
        gifsRecyclerView.setHasFixedSize(true)
        gifsRecyclerView.setItemViewCacheSize(25)

        updateItems()
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater)
        menuInflater.inflate(R.menu.fragment_gif_gallery, menu)

        val searchItem = menu.findItem(R.id.menu_item_search)
        val searchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String): Boolean {
                QueryPreferences.setStoredQuery(activity, s)
                updateItems()
                return true
            }

            override fun onQueryTextChange(s: String): Boolean {
                return false
            }
        })

        searchView.setOnSearchClickListener {
            val query = QueryPreferences.getStoredQuery(activity)
            searchView.setQuery(query, false)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (fetchItemsTask != null) {
            fetchItemsTask?.cancel(true)
        }
        imageDownloader.clearQueue()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.menu_item_clear -> {
                    QueryPreferences.setStoredQuery(activity, null)
                    updateItems()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }


    private fun setupAdapter(images: ArrayList<Gif>) {
        if (isAdded) {
            gifsRecyclerView.adapter = GifAdapter(images)
        }
    }

    private fun updateItems(offset: Int = 0) {
        val query = QueryPreferences.getStoredQuery(activity)
        FetchItemsTask(query, offset).execute()
    }

    private inner class FetchItemsTask(private val query: String?, val offset: Int)
        : AsyncTask<Void, Void, ArrayList<Gif>?>() {

        override fun onPreExecute() {
            fetchItemsTask = this
            if ((gifsRecyclerView.adapter?.itemCount ?: 0) == 0) {
                progressBar.visibility = View.VISIBLE
            }
        }

        override fun doInBackground(vararg params: Void): ArrayList<Gif>? {
            if (isCancelled) {
                return null
            }
            return if (query == null) {
                GifReceiver().fetchTrendingGifs(offset)
            } else {
                GifReceiver().searchGifs(query, offset)
            }
        }

        override fun onPostExecute(items: ArrayList<Gif>?) {
            if (isCancelled) {
                return
            }
            if (items != null) {
                if (offset > 0) {
                    (gifsRecyclerView.adapter as GifAdapter).appendItems(items)
                } else {
                    progressBar.visibility = View.GONE
                    setupAdapter(items)
                }
            } else {
                Snackbar.make(gifsRecyclerView, R.string.message_no_internet_connection, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.action_retry) {
                            updateItems(0)
                        }.show()
            }
            fetchItemsTask = null
        }
    }

    private inner class GifHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val gifImageView: GifImageView = itemView
                .findViewById<View>(R.id.fragment_photo_gallery_image_view) as GifImageView
        private val gifTitleTextView: TextView = itemView.findViewById<View>(R.id.tv_title) as TextView

        fun bindGalleryItem(galleryItem: Gif) {
            val gifImage = galleryItem.images?.fixedHeight
            gifImageView.prepareView(gifImage?.width ?: 0, gifImage?.height ?: 0)
            imageDownloader.queueImage(gifImageView, gifImage?.url)
            gifTitleTextView.text = galleryItem.title
        }
    }

    private inner class GifAdapter(private val galleryItems: ArrayList<Gif>) : RecyclerView.Adapter<GifHolder>() {

        override fun getItemCount(): Int {
            return galleryItems.size
        }

        override fun onCreateViewHolder(viewGroup: ViewGroup,
                                        viewType: Int): GifHolder {
            val inflater = LayoutInflater.from(activity)
            val view = inflater.inflate(R.layout.item_gallery, viewGroup,
                    false)
            return GifHolder(view)
        }

        override fun onBindViewHolder(gifHolder: GifHolder, position: Int) {
            val galleryItem = galleryItems[position]
            gifHolder.bindGalleryItem(galleryItem)
        }

        fun appendItems(items: ArrayList<Gif>) {
            for (i in items) {
                galleryItems.add(i)
                notifyItemInserted(galleryItems.size)
            }
        }
    }

}
