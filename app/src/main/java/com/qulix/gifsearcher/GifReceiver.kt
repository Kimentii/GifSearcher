package com.qulix.gifsearcher

import android.net.Uri
import android.util.Log
import com.google.gson.Gson
import com.qulix.gifsearcher.data.Gif
import com.qulix.gifsearcher.data.GiphyResponse
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket


class GifReceiver {

    companion object {
        private val TAG = GifReceiver::class.java.simpleName
        private const val API_KEY = "yHO63A43NP8YeplCWrIDOWSfvQMXiLta"
        private const val RESPONSE_FORMAT = "json"
        private const val LIMIT = "10"
        private val URI_TRENDING = Uri
                .parse("https://api.giphy.com/v1/gifs/trending")
                .buildUpon()
                .appendQueryParameter("api_key", API_KEY)
                .appendQueryParameter("fmt", RESPONSE_FORMAT)
                .appendQueryParameter("limit", LIMIT)
                .build()

        private val URI_SEARCH = Uri
                .parse("https://api.giphy.com/v1/gifs/search")
                .buildUpon()
                .appendQueryParameter("api_key", API_KEY)
                .appendQueryParameter("fmt", RESPONSE_FORMAT)
                .appendQueryParameter("limit", LIMIT)
                .build()
    }

    fun fetchTrendingGifs(offset: Int): ArrayList<Gif>? {
        if (!isOnline()) {
            return null
        }
        return downloadImages(URI_TRENDING.buildUpon()
                .appendQueryParameter("offset", offset.toString()).toString())
    }

    fun searchGifs(query: String, offset: Int): ArrayList<Gif>? {
        if (!isOnline()) {
            return null
        }
        return downloadImages(URI_SEARCH.buildUpon()
                .appendQueryParameter("q", query)
                .appendQueryParameter("offset", offset.toString()).toString())
    }

    fun getUrlBytes(urlString: String): ByteArray? {
        val client = OkHttpClient()
        val request: Request = Request.Builder()
                .url(urlString)
                .build()
        val response = client.newCall(request).execute()
        return response.body()?.bytes()
    }

    private fun getUrlString(urlString: String): String? {
        val bytes = getUrlBytes(urlString)
        return if (bytes != null) {
            String(bytes)
        } else {
            null
        }
    }

    private fun downloadImages(urlString: String): ArrayList<Gif> {
        val json = getUrlString(urlString)
        val gson = Gson()
        if (json != null) {
            val giphyResponse = gson.fromJson(json, GiphyResponse::class.java)
            return giphyResponse?.data ?: arrayListOf()
        }
        return arrayListOf()
    }

    private fun isOnline(): Boolean {
        return try {
            val timeoutMs = 1500
            val sock = Socket()
            val sockaddr = InetSocketAddress("8.8.8.8", 53)
            sock.connect(sockaddr, timeoutMs)
            sock.close()
            true
        } catch (e: IOException) {
            false
        }

    }
}
