package com.qulix.gifsearcher.data

class GiphyResponse {
    var data: ArrayList<Gif>? = null
    var pagination: Pagination? = null

    override fun toString(): String {
        val strBuilder = StringBuffer()
        strBuilder.append("Gifs num: " + (data?.size ?: "null")).append("\n")
        data?.forEach {
            strBuilder.append(it.images?.preview?.mp4 + "\n")
        }
        return strBuilder.toString()
    }
}
