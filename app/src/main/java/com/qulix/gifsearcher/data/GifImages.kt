package com.qulix.gifsearcher.data

import com.google.gson.annotations.SerializedName

class GifImages {
    var preview: GifImage? = null
    @SerializedName("fixed_height")
    var fixedHeight: GifImage? = null
}
