package com.qulix.gifsearcher.data

import com.google.gson.annotations.SerializedName

class Pagination {
    var offset: Int? = null
    @SerializedName("total_count")
    var totalCount: Int? = null
    var count: Int? = null
}
