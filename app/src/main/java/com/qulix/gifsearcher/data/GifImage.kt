package com.qulix.gifsearcher.data

class GifImage {
    val url: String? = null
    val width: Int? = null
    val height: Int? = null
    val size: Int? = null
    val mp4: String? = null
    val mp4_size: Int? = null
    val webp: String? = null
    val webp_size: Int? = null
}
