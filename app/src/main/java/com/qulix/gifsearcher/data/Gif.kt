package com.qulix.gifsearcher.data

import com.google.gson.annotations.SerializedName

class Gif {
    @SerializedName("type")
    var type: String? = null
    var id: String? = null
    var images: GifImages? = null
    var title: String? = null

}
