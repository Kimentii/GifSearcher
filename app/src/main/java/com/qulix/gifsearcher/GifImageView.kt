package com.qulix.gifsearcher

import android.content.Context
import android.graphics.Canvas
import android.graphics.Movie
import android.os.SystemClock
import android.util.AttributeSet
import android.view.View
import java.io.ByteArrayInputStream
import java.io.InputStream

class GifImageView : android.support.v7.widget.AppCompatImageView {

    var imageInputStream: InputStream? = null
        set(inputStream) {
            field = inputStream
            init()
        }
    private var movie: Movie? = null
    private var movieWidth: Int = 0
    private var movieHeight: Int = 0
    private var startTime: Long = 0

    constructor(context: Context) : super(context) {
        this.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
    }

    @JvmOverloads constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr) {
        if (attrs.getAttributeName(1) == "background") {
            val id = Integer.parseInt(attrs.getAttributeValue(1).substring(1))
            setImageResource(id)
        }
        this.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
    }

    fun prepareView(width: Int, height: Int) {
        imageInputStream = ByteArrayInputStream(ByteArray(0))
        movieWidth = width
        movieHeight = height
        init()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(movieWidth, movieHeight)
    }

    override fun onDraw(canvas: Canvas) {
        if (movie != null) {
            val now = SystemClock.uptimeMillis()

            if (startTime == 0L) {
                startTime = now
            }

            if (movie != null) {

                var duration = movie!!.duration()
                if (duration == 0) {
                    duration = 1000
                }
                val relTime = ((now - startTime) % duration).toInt()
                movie!!.setTime(relTime)
                movie!!.draw(canvas, 0f, 0f)
                invalidate()
            }
        }
    }

    private fun init() {
        isFocusable = true
        movie = Movie.decodeStream(imageInputStream)
        if (movie != null) {
            movieWidth = movie!!.width()
            movieHeight = movie!!.height()
        }
        requestLayout()
        invalidate()
    }
}
