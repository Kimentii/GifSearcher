package com.qulix.gifsearcher

import android.support.v4.app.Fragment

class MainActivity : SingleFragmentActivity() {

    override fun createFragment(): Fragment {
        return GifGalleryFragment.newInstance()
    }
}
