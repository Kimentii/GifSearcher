package com.qulix.gifsearcher

import android.os.Handler
import android.os.HandlerThread
import android.os.Message
import android.util.Log
import android.util.LruCache
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.InputStream
import java.util.concurrent.ConcurrentHashMap

class ImageDownloader<T>(private val mResponseHandler: Handler) : HandlerThread(TAG) {

    var imageDownloadListener: ((T, InputStream) -> Unit)? = null
    private var requestHandler: Handler? = null
    private val requestMap = ConcurrentHashMap<T, String>()
    private val memoryCache: LruCache<String, ByteArray>

    companion object {
        private val TAG = ImageDownloader::class.java.simpleName
        private const val MESSAGE_DOWNLOAD = 0
    }

    init {
        val maxMemory = (Runtime.getRuntime().maxMemory() / 1024).toInt()
        val cacheSize = maxMemory / 8

        memoryCache = object : LruCache<String, ByteArray>(cacheSize) {
            override fun sizeOf(key: String, byteArray: ByteArray): Int {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return byteArray.count() / 1024
            }
        }
    }

    override fun onLooperPrepared() {
        requestHandler = object : Handler() {
            override fun handleMessage(msg: Message) {
                if (msg.what == MESSAGE_DOWNLOAD) {
                    val target = msg.obj as T
                    handleRequest(target)
                }
            }
        }
    }

    fun queueImage(target: T, url: String?) {
        if (url == null) {
            requestMap.remove(target)
        } else {
            requestMap[target] = url
            requestHandler!!.obtainMessage(MESSAGE_DOWNLOAD, target)
                    .sendToTarget()
        }
    }

    fun clearQueue() {
        requestHandler!!.removeMessages(MESSAGE_DOWNLOAD)
    }

    private fun addByteArrayToMemoryCache(key: String, byteArray: ByteArray) {
        if (getByteArrayFromMemCache(key) == null) {
            memoryCache.put(key, byteArray)
        }
    }

    private fun getByteArrayFromMemCache(key: String): ByteArray? {
        return memoryCache.get(key)
    }

    private fun handleRequest(target: T) {
        try {
            val url = requestMap[target] ?: return
            val byteArrayFromCache = getByteArrayFromMemCache(url)
            if (byteArrayFromCache == null) {
                val gifBytes = GifReceiver().getUrlBytes(url)
                if (gifBytes != null) {
                    addByteArrayToMemoryCache(url, gifBytes)
                    mResponseHandler.post(Runnable {
                        if (requestMap[target] != url) {
                            return@Runnable
                        }

                        requestMap.remove(target)
                        imageDownloadListener?.invoke(target, ByteArrayInputStream(gifBytes))
                    })
                }
            } else {
                mResponseHandler.post(Runnable {
                    if (requestMap[target] != url) {
                        return@Runnable
                    }

                    requestMap.remove(target)
                    imageDownloadListener?.invoke(target, ByteArrayInputStream(byteArrayFromCache))
                })
            }
        } catch (ioe: IOException) {
            Log.e(TAG, "Error downloading image", ioe)
        }

    }
}
